class Api::V1::Notebooks::ApplicationController < Api::V1::ApplicationController

  protected

  def resource_notebook
    @notebook ||= Notebook.find(params[:notebook_id])
  end
end
