class Api::V1::Notebooks::NotesController < Api::V1::Notebooks::ApplicationController
  def index
    @notes = Note.all

    render json: @notes
  end

  def create
    @note = Note.new
    @form = ::Api::V1::Notebooks::NoteForm.new(@note)

    if @form.validate(params[:note])
      @form.save
    end

    respond_with @note, location: nil
  end

  def update
    @note = Note.find(params[:id])
    @form = ::Api::V1::Notebooks::NoteForm.new(@note)

    if @form.validate(params[:note])
      @form.save
    end

    respond_with @notebook, location: nil
  end

  def destroy
    @note = Note.find(params[:id])

    @note.destroy

    render nothing: true, status: :ok
  end
end
