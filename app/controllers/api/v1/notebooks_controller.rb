class Api::V1::NotebooksController < Api::V1::ApplicationController
  def index
    @notebooks = Notebook.all

    render json: @notebooks
  end

  def create
    @notebook = Notebook.new
    @form = ::Api::V1::NotebookForm.new(@notebook)

    if @form.validate(params[:notebook])
      @form.save
    end

    respond_with @notebook, location: nil
  end

  def update
    @notebook = Notebook.find(params[:id])
    @form = ::Api::V1::NotebookForm.new(@notebook)

    if @form.validate(params[:notebook])
      @form.save
    end

    respond_with @notebook, location: nil
  end

  def destroy
    @notebook = Notebook.find(params[:id])

    @notebook.destroy

    render nothing: true, status: :ok
  end
end
