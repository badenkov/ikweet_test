class Api::V1::MessagesController < Api::V1::ApplicationController
  def index
    @messages = Message.all

    render json: @messages
  end

  def create
    @message = Message.new
    @form = ::Api::V1::MessageForm.new(@message)

    if @form.validate(params[:message])
      @form.save
    end

    respond_with @message, location: nil
  end
end
