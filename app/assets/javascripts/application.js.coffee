#= require_self
#= require jquery
#= require jquery_ujs
#= require lodash
#= require turbolinks
#= require react
#= require react_ujs
#= require reflux

#= require_tree ./actions
#= require_tree ./stores
#= require_tree ./components

if typeof global == "undefined" 
  global = window
  window.global = global
