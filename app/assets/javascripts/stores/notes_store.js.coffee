global.NotesStore = Reflux.createStore
  init: ->
    @notebook = null
    @items = []

    @listenTo NotesActions.setNotebook, @onSetNotebook
    @listenTo NotesActions.getAll, _.throttle(@onGetAll, 2000)
    @listenTo NotesActions.create, @onCreate
    @listenTo NotesActions.update, @onUpdate
    @listenTo NotesActions.destroy, @onDestroy

  onSetNotebook: (notebook) ->
    console.log(notebook)
    @notebook = notebook
    NotesActions.getAll()
    @trigger("ok")

  onGetAll: ->
    if @notebook == null
      return

    $.ajax
      url: "/api/v1/notebooks/#{@notebook}/notes.json"
      success: (data) =>
        @items = data
        @trigger("ok")

  onCreate: (data) ->
    $.ajax
      url: "/api/v1/notebooks/#{@notebook}/notes.json"
      type: "POST"
      data: 
        note: data
      success: (response) =>
        @items = response

        NotesActions.getAll(@notebook)
        @trigger("created")
    

  onUpdate: (id, data) ->
    $.ajax
      url: "/api/v1/notebooks/#{@notebook}/notes/#{id}.json"
      type: "PUT"
      data:
        note: data
      success: (response) =>
        @items = response

        NotesActions.getAll(@notebook)

  onDestroy: (id) ->
    $.ajax
      url: "/api/v1/notebooks/#{@notebook}/notes/#{id}.json"
      type: "DELETE"
      success: (data) =>
        NotesActions.getAll()
        @trigger("deleted")
