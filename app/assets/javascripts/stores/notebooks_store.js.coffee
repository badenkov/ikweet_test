global.NotebooksStore = Reflux.createStore
  init: ->
    @items = {}
    @pageCount = 0
    @currentPage = 1

    @listenTo NotebooksActions.getAll, _.throttle(@onGetAll, 2000)
    @listenTo NotebooksActions.create, @onCreate
    @listenTo NotebooksActions.update, @onUpdate
    @listenTo NotebooksActions.destroy, @onDestroy
  
  onGetAll: ->
    $.ajax
      url: "/api/v1/notebooks.json"
      data:
        page: @currentPage
      success: (data) =>
        @items = data
        @trigger("ok")

  onCreate: (title) ->
    $.ajax
      url: "/api/v1/notebooks.json"
      type: "POST"
      data:
        notebook:
          title: title
      success: (data) =>
        NotebooksActions.getAll()
        @trigger("created")

  onUpdate: (id, title) ->
    $.ajax
      url: "/api/v1/notebooks/#{id}.json"
      type: "PUT"
      data:
        notebook:
          title: title
      success: (data) =>
        NotebooksActions.getAll()
        @trigger("updated")

  onDestroy: (id) ->
    $.ajax
      url: "/api/v1/notebooks/#{id}.json"
      type: "DELETE"
      success: (data) =>
        NotebooksActions.getAll()
        @trigger("deleted")
