TodoActions =
  create: Reflux.createAction()
  update: Reflux.createAction()
  destroy: Reflux.createAction()

window.TodoActions = TodoActions

TodoStore = Reflux.createStore
  init: ->
    @items = {}

    @listenTo TodoActions.create, @create
    @listenTo TodoActions.update, @update
    @listenTo TodoActions.destroy, @destroy

  create: (text) ->
    id = Date.now()
    @items[id] =
      id: id
      complete: false
      text: text

    @trigger @items

  update: (id, text, complete=false) ->
    @items[id] =
      id: id
      complete: complete
      text: text

    @trigger @items

  destroy: (id) ->
    delete @items[id]

    @trigger @items

  getText: ->
    m = _.map(@items, (v, k) ->
      v.text
    )
    result = m.join(",\n")
    result

window.TodoStore = TodoStore

TodoApp = React.createClass
  getInitialState: ->
    items: {}

  componentDidMount: ->
    @unsubscribe = TodoStore.listen(@_onItemsChange)

  componentWillUnmount: ->
    @unsubscribe()

  render: ->
    React.DOM.div {}, [
      React.DOM.ul {}, [
        for key, item of @state.items
          React.DOM.li {}, [
            item.text,
            React.DOM.button { onClick: @remove.bind(this, key) }, "Удалить"
          ]

      ]
      React.DOM.textarea({
        value: TodoStore.getText()
      }, [
        TodoStore.getText()
      ])
    ]

  _onItemsChange: (items) ->
    @setState
      items: items

  remove: (key) ->
    TodoActions.destroy(key)

window.TodoApp = TodoApp
