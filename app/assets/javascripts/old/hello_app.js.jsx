/** @jsx React.DOM */
var HelloApp = React.createClass({
  getInitialState: function() {
    //this.loadMessages();

    return {
      text: "lkj",
      messages: this.props.messages
    };                                                                     
  },

  componentDidMount: function() {
    //this.loadMessages();
  },

  loadMessages: function() {
    var component = this;
    jQuery
      .get('/api/v1/messages.json')
      .then(function(data) {
        component.setState({ messages: data });
      });
  },
                     
  handleChange: function(e) {
    this.setState({ text: e.target.value });
  },

  handleSubmit: function(e) {
    e.preventDefault();

    var q = this.state.text;
    this.setState({ text: "" });

    //var messages = this.state.messages;
    //messages.push(q);
    //this.setState({ messages: messages });

    var component = this;
    var data = { message: { text: q } };
    jQuery.post('/api/v1/messages.json', data).then(function(data) {
      component.loadMessages();
    });
  },

  render: function() {
    var createMessage = function(message) {
      return <p>{message.text}</p>
    };

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-4">
            <form className="form" onSubmit={this.handleSubmit}>
              <div className="form-group">
                <input className="form-control" onChange={this.handleChange} value={this.state.text} />
              </div>
              <button className="btn btn-success" type="submit">Send</button>
            </form>
          </div>
          <div className="col-md-8">
            <div className="messages">
              {_.map(this.state.messages, createMessage)}
            </div>
            <p>{this.state.messages.length} запросов</p>
          </div>
        </div>
      </div>
    );
  }
});
