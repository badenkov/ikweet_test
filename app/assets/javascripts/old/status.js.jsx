/** @jsx React.DOM */

var StatusActions = {
  create: Reflux.createAction(),
  update: Reflux.createAction(),
  destroy: Reflux.createAction()
};

var StatusStore = Reflux.createStore({
  init: function() {
    this.listenTo(StatusActions.update, this.output);
  },

  output: function(flag) {
    status = flag ? 'ONLINE' : 'OFFLINE';

    this.trigger(status);
  }
});
    

var Status = React.createClass({
  getInitialState: function() {
    return {
      currentStatus: "OFFLINE"
    };
  },

  componentDidMount: function() {
    this.unsubscribe = StatusStore.listen(this.onStatusChange);
  },

  componentWillUnmount: function() {
    this.unsubscribe();
  },

  onStatusChange: function(status) {
    this.setState({
      currentStatus: status
    });
  },

  online: function() {
    StatusActions.update(true)
  },
  offline: function() {
    StatusActions.update(false)
  },

  render: function() {
    return (
      <div>
        <h1>{this.state.currentStatus}</h1>
        <button onClick={this.online}>Online</button>
        <button onClick={this.offline}>Offline</button>
      </div>
    );
  }
});

var Status1 = React.createClass({
  getInitialState: function() {
    return {
      currentStatus: "OFFLINE"
    };
  },

  componentDidMount: function() {
    this.unsubscribe = StatusStore.listen(this.onStatusChange);
  },

  componentWillUnmount: function() {
    this.unsubscribe();
  },

  onStatusChange: function(status) {
    this.setState({
      currentStatus: status
    });
  },

  render: function() {
    return (
      <div>
        <h1>Ваш Статус <strong>{this.state.currentStatus}</strong></h1>
      </div>
    );
  }
});

window.Status = Status;
