global.Notebooks = React.createClass
  propTypes:
    items: React.PropTypes.array
    pageCount: React.PropTypes.number
    currentPage: React.PropTypes.number
    currentNotebook: React.PropTypes.number

  getDefaultProps: ->
    items: []
    pageCount: 0
    currentPage: 1
    currentNotebook: null

  getInitialState: ->
    items: @props.items
    pageCount: @props.pageCount
    currentPage: @props.currentPage
    currentNotebook: @props.currentNotebook

    editedItem: null

  componentDidMount: ->
    @notebooksStoreUnsubscribe = NotebooksStore.listen(@_onNotebooksStoreChange)
    @notesStoreUnsubscribe = NotesStore.listen(@_onNotesStoreChange)

    NotebooksActions.getAll()

  componentWillUnmount: ->
    @notebooksStoreUnsubscribe()
    @notesStoreUnsubscribe()

  _onNotebooksStoreChange: (state) ->
    console.log("RESPONSE: #{state}")
    @setState
      items: NotebooksStore.items
      pageCount: NotebooksStore.pageCount
      currentPage: NotebooksStore.currentPage

  _onNotesStoreChange: (state) ->
    console.log("RESPONSE: #{state}")
    @setState
      currentNotebook: NotesStore.notebook

  _setCurrentNotebook: (id) ->
    NotesActions.setNotebook(id)

  _create: (title) ->
    NotebooksActions.create(title)

  _setEditedItem: (id) ->
    @setState(editedItem: id)

  _update: (id, title) ->
    @setState(editedItem: null)
    NotebooksActions.update(id, title)

  _destroy: (id) ->
    NotebooksActions.destroy(id)

  render: ->
    React.DOM.div null,
      React.DOM.div null, "Количество #{@state.items.length}"
      React.DOM.div null, "Текущий блокнот #{@state.currentNotebook}"
      React.DOM.ul null,
        for item in @state.items
          if @state.editedItem == item.id
            React.DOM.li null,
              NotebookForm title: item.title, onSubmit: @_update.bind(this, item.id)
              React.DOM.a onClick: @_destroy.bind(this, item.id), "Remove"
          else
            #React.DOM.li onClick: @_destroy.bind(this, item.id), item.title
            React.DOM.li onClick: @_setCurrentNotebook.bind(this, item.id),
              item.title
              React.DOM.a onClick: @_setEditedItem.bind(this, item.id),
                "Редактировать"

      React.DOM.hr null
      NotebookForm title: "", onSubmit: @_create

