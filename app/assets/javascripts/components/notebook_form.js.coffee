global.NotebookForm = React.createClass
  mixins: [React.addons.LinkedStateMixin]
  propTypes:
    title: React.PropTypes.string
    onSubmit: React.PropTypes.func.isRequired

  getDefaultProps: ->
    title: "Название вашего блокнота"

  getInitialState: ->
    title: @props.title

  handleSubmit: (event) ->
    @props.onSubmit(@state.title)
    @setState(title: @props.title)

    return false

  render: ->
    React.DOM.form onSubmit: @handleSubmit,
      React.DOM.input type: "text", valueLink: @linkState('title')
      React.DOM.button type: "submit", "Save"
