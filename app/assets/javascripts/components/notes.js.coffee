global.Notes = React.createClass
  propTypes:
    notebook: React.PropTypes.number
    items: React.PropTypes.array

  getDefaultProps: ->
    notebook: null
    items: []

  getInitialState: ->
    notebook: @props.notebook
    items: @props.items

  componentDidMount: ->
    @notesStoreUnsubscribe = NotesStore.listen(@_onNotesStoreChange)

  componentWillUnmount: ->
    @notesStoreUnsubscribe()

  _onNotesStoreChange: (state) ->
    console.log("RESPONSE: #{state}")
    @setState
      notebook: NotesStore.notebook
      items: NotesStore.items

  render: ->
    React.DOM.div null,
      React.DOM.div null, "Количество #{@state.items.length}"
      React.DOM.div null, "Блокно #{@state.notebook}"
      React.DOM.ul null,
        for item in @state.items
          React.DOM.li null, item.content
