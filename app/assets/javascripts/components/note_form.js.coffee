global.NoteForm = React.createClass
  mixins: [React.addons.LinkedStateMixin]
  propTypes:
    content: React.PropTypes.string

  getDefaultProps: ->
    content: ""

  getInitialState: ->
    content: @props.content

  handleSubmit: (event) ->
    NotesActions.create
      content: @state.content

    @setState
      content: ""

    return false

  render: ->
    React.DOM.form onSubmit: @handleSubmit,
      React.DOM.textarea valueLink: @linkState("content")
      React.DOM.button type: "submit", "Save"
