global.NotebooksActions =
  getAll: Reflux.createAction()
  create: Reflux.createAction()
  update: Reflux.createAction()
  destroy: Reflux.createAction()
