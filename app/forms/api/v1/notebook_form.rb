class Api::V1::NotebookForm < Reform::Form
  include Reform::Form::ActiveRecord

  property :title
  validates :title, presence: true
end
