class Api::V1::Notebooks::NoteForm < Reform::Form
  include Reform::Form::ActiveRecord

  property :content
  validates :content, presence: true
end
