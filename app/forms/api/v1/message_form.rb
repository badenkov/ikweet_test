class Api::V1::MessageForm < Reform::Form
  include Reform::Form::ActiveRecord
  
  property :text
  validates :text, presence: true
end
