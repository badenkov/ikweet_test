Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      resources :messages, only: [:index, :create]
      resources :notebooks, only: [:index, :create, :update, :destroy] do
        scope module: :notebooks do
          resources :notes, only: [:index, :create, :update, :destroy]
        end
      end
    end
  end

  scope module: :web do
    root to: "welcome#index"
  end
end
