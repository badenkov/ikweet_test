# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

set :user, 'hosting_badenkov'
set :deploy_server, "sulfur.locum.ru"
set :deploy_to, "/home/#{fetch(:user)}/projects/#{fetch(:application)}"

#role :app, %w{deploy@example.com}
#role :web, %w{deploy@example.com}
#role :db,  %w{deploy@example.com}

role :app, fetch(:deploy_server)
role :web, fetch(:deploy_server)
role :db, fetch(:deploy_server)
#role :web, deploy_server
#role :db, deploy_server


# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

server fetch(:deploy_server), user: fetch(:user), roles: %w{web app}, my_property: :my_value


# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
# And/or per server (overrides global)
# ------------------------------------
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }

set :rails_env, "production"

set :unicorn_conf, "/etc/unicorn/#{fetch(:application)}.#{fetch(:login)}.rb"
set :unicorn_pid, "/var/run/unicorn/#{fetch(:application)}.#{fetch(:login)}.pid"
#set :unicorn_start_cmd, "(cd #{fetch(:deploy_to)}/current; rvm use #{fetch(:rvm_ruby_version)} do bundle exec unicorn_rails -Dc #{fetch(:unicorn_conf)})"

set :bundle_bins, fetch(:bundle_bins, []).push(:unicorn_rails)
set :rvm_map_bins, fetch(:rvm_map_bins, []).push(:unicorn_rails)

namespace :deploy do

  task :symlink_db_config do
    on roles(:app), in: :sequence do
      execute :ln, '-nfs', release_path.join('config/database.yml.sample'), release_path.join('config/database.yml')
    end
  end

  after :updating, :symlink_db_config

  task :start do
    on roles(:app), in: :sequence do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :unicorn_rails, "-Dc#{fetch(:unicorn_conf)}"
        end
      end
    end
  end

  task :stop do
    on roles(:app), in: :sequence do
      if test "[ -f #{fetch(:unicorn_pid)} ]" 
        execute :kill, "-QUIT `cat #{fetch(:unicorn_pid)}`"
      end
    end
  end

  task :restart do
    on roles(:app), in: :sequence do
      invoke :"deploy:stop"
      invoke :"deploy:start"
    end
  end

  after :deploy, :restart
end
