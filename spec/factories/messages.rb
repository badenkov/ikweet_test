# == Schema Information
#
# Table name: messages
#
#  id         :integer          not null, primary key
#  text       :string(255)
#  created_at :datetime
#  updated_at :datetime
#
FactoryGirl.define do
  factory :message do
    text
  end
end
