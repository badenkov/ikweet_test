FactoryGirl.define do
  sequence :title do |n|
    "title_#{n}"
  end

  sequence :url do |n|
    "http://example_#{n}.com"
  end

  sequence :text do |n|
    "text_#{n}"
  end
end
