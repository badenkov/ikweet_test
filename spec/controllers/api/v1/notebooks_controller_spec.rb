require 'spec_helper'

describe Api::V1::NotebooksController, type: :controller do
  describe "GET #index" do
    it "responds success" do
      get :index, format: :json

      expect(response).to be_success
      expect(response.status).to eq(200)
    end
  end

  describe "POST #create" do
    it "create new notebook" do
      attrs = attributes_for :notebook
      
      post :create, notebook: attrs, format: :json

      expect(response).to be_success
      expect(response.status).to eq(201)
    end
  end
end
