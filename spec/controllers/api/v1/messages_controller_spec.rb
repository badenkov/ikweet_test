require 'spec_helper'

describe Api::V1::MessagesController, :type => :controller do

  describe "GET #index" do
    it "responds success" do
      get :index, format: :json

      expect(response).to be_success
      expect(response.status).to eq(200)
    end
  end

  describe "POST #create" do
    it "creates new message" do
      attrs = attributes_for :message

      post :create, message: attrs, format: :json

      expect(response).to be_success
      expect(response.status).to eq(201)
    end
  end

end
