source 'https://rubygems.org'
source 'https://rails-assets.org'

gem 'rails', '4.1.1'
gem 'unicorn'
gem 'racer'
gem 'pg'
gem 'reform'
gem 'bcrypt', '~> 3.1.7'
gem 'configus'
gem 'haml'
gem 'sass-rails', '~> 4.0.3'
gem 'coffee-rails', '~> 4.0.0'
gem 'therubyracer',  platforms: :ruby
gem 'uglifier', '>= 1.3.0'

gem 'bootstrap-sass'
gem 'jquery-rails'
gem 'turbolinks'
gem 'react-rails', github: 'reactjs/react-rails', branch: :master
gem 'lodash-rails'

gem 'rails-assets-reflux'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

group :development, :test do
  gem 'pry-rails'
  gem 'pry-doc'
  gem 'pry-git'
  gem 'pry-stack_explorer'
  gem 'awesome_print'
  gem 'hirb'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'rspec-rails', '~> 3.0.0.rc1'
end

group :development do
  gem 'thin'
  gem 'sqlite3'
  gem 'spring'
  gem 'spring-commands-rspec'
  gem 'annotate'

  gem 'foreman'

  gem 'capistrano'
  gem 'capistrano-rvm'
  gem 'capistrano-bundler'
  gem 'capistrano-rails'
end

group :test do
  gem 'factory_girl_rails'
  gem 'simplecov', require: false
end
